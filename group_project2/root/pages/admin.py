from django.contrib import admin
from . models import Page

orderlist = ["title"]
searchlist = ["title"]

class PageAdmin(admin.ModelAdmin):
    list_display = ('title','update_date')
    ordering = (orderlist)
    search_fields = (searchlist)

admin.site.register(Page, PageAdmin)

# Register your models here.
